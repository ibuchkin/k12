(function words()
{

var WordItem = function WordItem( sound, text )
    {
    this.sound = sound || "ouch";
    this.text = (_.isString(text) && text) || (_.isArray(text) && _.sample(text)) || this.sound;
    if ( this.text[0] == '_' )
        this.text = this.text.substr(1);
    };


var WordsVM = function WordsVM()
    {
    var self = this;
    self.playOn = ko.observable( 0 );
    self.startTime = ko.observable( null );
    self.resultsLog = ko.observableArray( [] );
    self.timeLeft = ko.observable( "00" );
    self.expected = ko.observable( null );
    self.expectedText = ko.computed( function() { return (self.expected() || {}).text; } );
    self.words = ko.observable( [] );
    self.wordsLeft = ko.computed( function() { return _.filter( self.words() || [], even_ ); } );
    self.wordsRight = ko.computed( function() { return _.reject( self.words() || [], even_ ); } );
    self.scorePlus = ko.computed( function() { return _.where( self.resultsLog(), { ok: true } ).length; } );
    self.scoreFrom = ko.computed( function() { return self.resultsLog().length; } );

    function checkTimeLeft_()
        {
        var passed = Math.floor( (new Date() - self.startTime()) / 1000 );
        return passed > kSeconds ? 0 : kSeconds - passed;
        };
    function startWatch_()
        {
        var left = checkTimeLeft_();
        self.timeLeft( ("00" + left).slice( -2 ) );
        if ( left )
            _.delay( startWatch_, 300 );
        };

    function wordClick_( w )
        {
        //alert( w.toSource() );
        var ok = w.text == self.expectedText();
        self.resultsLog.unshift( { word: w.text, ok: ok, expected: self.expectedText() } );
        if ( checkTimeLeft_() )
            self.nextWord();
        else
            self.finish()
        };
    self.play = function play()
        {
        var sound = self.expected().sound;
        ///$("#player").attr( "src", "play.html?text="+ sound );
        var audioNode = document.createElement("audio");// $("#player").attr( "src", "play.html?text="+ sound );
        audioNode.src = "./play/" + sound + ".mp3";
        audioNode.play();
        };

    function even_( x, i ) { return 0 == i % 2; };
    function makeItem_( x ) { return _.extend( new WordItem( x[0], x[1] ), { click: wordClick_ } ); };

    self.nextWord = function nextWord()
        {
        //alert( _.chain( kWords ).pairs().sample( kMaxWords ).map( makeItem_ ).value().toSource() );
        self.words( _.chain( kWords ).pairs().sample( kMaxWords ).map( makeItem_ ).value() );
        self.expected( _.sample( self.words() ) );
        self.play();// self.expected() );
        };
    self.start = function start()
        {
        self.resultsLog( [] );
        self.startTime( new Date() );
        self.playOn( 1 );
        _.delay( startWatch_, 300 );
        self.nextWord();
        };
    self.finish = function finish()
        {
        self.playOn( 0 );
        };
    };


var vm = new WordsVM();
ko.applyBindings( vm );
// vm.start();

}) ();

