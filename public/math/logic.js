
(function()
{
var global = this;

function _rand( n )
    {
    return Math.floor( 1000000 * Math.random() ) % n;
    }

function randPick( arr )
    {
    return arr[ _rand( arr.length ) ];
    }

this.MathVM = function MathVM() 
    {
    var self = this;

    this.subj = ko.observable( "Masha" );
    this.subjPronoun = ko.observable( "her" );
    this.initQty = ko.observable(0);
    this.delta = ko.observable(0);
    this.deltaStr = ko.computed( function() { return '' + (self.delta() < 0 ? -self.delta() : self.delta()); } );
    this.answerExpected = ko.computed( function() { return self.initQty() + self.delta(); } );
    this.answerEntered = ko.observable( "" );
    this.operator = ko.observable( "A fairy" );
    this.operationStr = ko.observable( "added" );
    this.operationModifier = ko.computed( function() { return (self.delta() < 0 ? "of the" : "more"); } );
    this.objects = ko.observable( "apples" );
    this.result = ko.observable( "" );
    this.goodbad = ko.observable( "good" );

    this.checkAnswer = function checkAnswer()
        {
        var answer = parseInt( self.answerEntered()+'' );
        if ( answer == self.answerExpected() )
            { self.result( ':-) <br/><br/>Yes! It is '+ answer ); self.goodbad( "good" ); }
        else
            { self.result( ':-( <br/><br/>NO, not '+ (isNaN(answer) ? "<i>that</i>" : answer) +'<br/>Think again'); self.goodbad( "bad" ); }
        };

    this.reset = function reset()
        {
        self.result( "" );
        self.answerEntered( "" );

        var subjIndex = _rand( kSubj.length );
        self.subj( kSubj[ subjIndex ] );
        self.subjPronoun( ["him","her"][ subjIndex%2 ] );
        self.objects( randPick( kObjects ) );
        var isAdd = _rand( 2 );
        self.operator( isAdd ? kAdders : kSubtractors );
        if ( isAdd )
            {
            self.initQty( 2 + _rand( 9 ) );
            self.delta( 2 + _rand( 7 ) );
            }
        else
            {
            self.initQty( 5 + _rand( 9 ) );
            self.delta( 0 - 1 - _rand( self.initQty()-1 ) );
            self.subjPronoun( "" ); //"from " +  self.subjPronoun() );
            }
        self.operator( randPick( isAdd ? kAdders : kSubtractors ) );
        self.operationStr( randPick( isAdd ? kAdditions : kSubtractions ) );
        };

    this.renew = function renew()
        {
        // global.window.location = global.window.location+'';
        this.reset();
        };

    this.answerAppend = function( str )
        {
        self.answerEntered( self.answerEntered() + str );
        };

    this.answerBackspace = function( str )
        {
        self.answerEntered( self.answerEntered().slice( 0, -1 ) );
        };

    var buttons = [];
    for ( var i = 1; i <= 10; ++i ) buttons.push( { click: self.answerAppend.bind( self, i % 10 ), label: ''+(i%10) } );
    buttons.push( { click: self.answerBackspace.bind( self ), label: "DEL" } );
    this.buttons = ko.observableArray( buttons );
    };

var model = new MathVM();
ko.applyBindings( model );
model.reset();

})();
