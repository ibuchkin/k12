
(function()
{
var global = this;

global.kSubj = [ "Andrei", "Masha", "Fat Cat Simon", "Sleepy Julia", "David", "Rapunzel", "Peter Pan", "Rainbow Dash", "Very sleepy Ilya", "Daisy Jo" ];
global.kObjects = [ "apples", "pretend dinosaur eggs", "bananas", "crayons", "pine cones"
               , "raspberries", "wooden sticks", "carrots", "cherries", "romaine leaves", "little pickles" ];
global.kAdders = [ "A magic fairy", "Angry Birds kindly", "Little ponies", "Paleo Pals", "Friends", "Fluttershy", "Serendipity" ];
global.kSubtractors = [ "Evil Witch", "Red Ants", "A very hungry caterpillar", "Paleo Pals", "Silly penguins", "A specially trained mouse", "One mean monkey" ];
global.kAdditions = [ "brought", "gave", "sent", "bought" ];  // "made",
global.kSubtractions = [ "ate", "took away", "borrowed", "composted"
                    , "boiled and then ate", "microwaved and then ate", "made soup out of", "made salad out of"
                    ];
})();

